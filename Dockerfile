FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim
COPY build/libs/weather-report-app-0.1-all.jar weather-report-app.jar
EXPOSE 8081
ENV MICRONAUT_SERVER_PORT=8081
CMD java -noverify ${JAVA_OPTS} -jar weather-report-app.jar
