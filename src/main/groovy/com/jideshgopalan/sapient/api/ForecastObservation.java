package com.jideshgopalan.sapient.api;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY)
public class ForecastObservation {
    private String forecastDate;
    private double windSpeed;
    private double avgTemparature;
    private double minTemperature;
    private double maxTemperature;
    private double precipitationPropability;
    private double avgCloudCoverage;

    @JsonSetter("forecast_date")
    public String getForecastDate() {
        return forecastDate;
    }

    @JsonSetter("valid_date")
    public void setForecastDate(String forecastDate) {
        this.forecastDate = forecastDate;
    }

    @JsonProperty("wind_spd")
    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed * 3.6;
    }

    @JsonProperty("temp")
    public void setAvgTemparature(double avgTemparature) {
        this.avgTemparature = avgTemparature;
    }

    @JsonProperty("min_temp")
    public void setMinTemperature(double minTemperature) {
        this.minTemperature = minTemperature;
    }

    @JsonProperty("max_temp")
    public void setMaxTemperature(double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    @JsonProperty("pop")
    public void setPrecipitationPropability(double precipitationPropability) {
        this.precipitationPropability = precipitationPropability;
    }

    @JsonProperty("clouds")
    public void setAvgCloudCoverage(double avgCloudCoverage) {
        this.avgCloudCoverage = avgCloudCoverage;
    }

    @JsonProperty("message")
    public String getMessage() {
        if(this.precipitationPropability > 50) {
            return "Carry umbrella'";
        }else if(avgTemparature > 40){
            return "Use sunscreen lotion";
        }else{
            return "Its a pleasant day!";
        }
    }


    @Override
    public String toString() {
        return "  - average temperature: " + avgTemparature + "°C" +
                "\n  - min temperature: " + minTemperature + "°C" +
                "\n  - max temperature: " + maxTemperature + "°C" +
                "\n  - wind speed: " + windSpeed + " km/h" +
                "\n  - probability of precipitation: " + precipitationPropability + "%" +
                "\n  - average cloud coverage: " + avgCloudCoverage + "%";
    }
}
