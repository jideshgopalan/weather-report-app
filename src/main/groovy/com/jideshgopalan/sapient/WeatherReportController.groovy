package com.jideshgopalan.sapient

import com.fasterxml.jackson.core.Version
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.jideshgopalan.sapient.api.ForecastObservation
import com.jideshgopalan.sapient.api.ForecastResponse
import com.jideshgopalan.sapient.api.WeatherAPIClient
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.jackson.ObjectMapperFactory
import io.micronaut.jackson.convert.ObjectToJsonNodeConverter

import javax.inject.Inject

/**
 * @author Jidesh Gopalan
 * @since 1.0
 */
@Controller("/")
class WeatherReportController {

    @Inject
    WeatherAPIClient weatherAPIClient;

    @Get("/home")
    String home(){
        return "weather report service 12"
    }

    @Get("/weather/{country}/{city}/{days}")
    String weatherReport(String country, String city, String days) {
        if(country==null || city == null || days == null){
            return "invalid url; url format should be /weather/{country}/{city}/{days}"
        }
        try{
            ForecastResponse forecast = weatherAPIClient.forecast(city, country, Integer.parseInt(days))
            ObjectMapper mapper = new ObjectMapper()
            return mapper.writeValueAsString(forecast.data)
        }catch(Exception e){
            return "No data found for the city ${country}/{$city}/{$days}"
        }

    }
}
