micronaut {
    application {
        name = "weather-report-app"
    }
    executors {
        io {
            type = "fixed"
            nThreads = 75
        }
    }
}
weather {
    api {
        url = "https://api.weatherbit.io/v2.0/"
        key = "dae94f137655498fa9b0d29288f1d9a7"
    }
}
