package com.jideshgopalan.sapient

import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client

/**
 * @author graemerocher
 * @since 1.0
 */
@Client('/')
interface WeatherReportClient {

    @Get("/weather/{country}/{city}/{days}")
    String weatherReport(String country, String city, String days)
}
