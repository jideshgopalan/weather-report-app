package com.jideshgopalan.sapient
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

/**
 * @author jideshgopalan
 */
@MicronautTest
class WeatherReportControllerSpec extends Specification {

    @Inject
    WeatherReportClient client

    void "test weather report"() {
        expect:
        client.weatherReport("India", "Delhi", "3").contains("message")
    }
}
